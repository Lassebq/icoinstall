#ifndef BUFFUTILS_H
#define BUFFUTILS_H

#include <stdlib.h>

typedef struct {
	void *usrptr;
	void (*write)(void *, void *buf, size_t n);
	void (*close)(void *);
} WriteCallbacks;

typedef struct {
	void *usrptr;
	int (*read)(void *, void *buf, size_t n);
	void (*seek)(void *, size_t n, int whence);
	void (*close)(void *);
} ReadCallbacks;

void buffutils_memfd_write(void *usrptr, void *buf, size_t n);
int buffutils_memfd_read(void *usrptr, void *buf, size_t n);
void buffutils_memfd_seek(void *usrptr, size_t n, int whence);
void buffutils_memfd_close(void *usrptr);

#define memfd_writeback(memfile) {.usrptr = memfile, .write = buffutils_memfd_write, .close = buffutils_memfd_close}
#define memfd_readback(memfile) {.usrptr = memfile, .read = buffutils_memfd_read, .seek = buffutils_memfd_seek, .close = buffutils_memfd_close}

void buffutils_file_write(void *usrptr, void *buf, size_t n);
int buffutils_file_read(void *usrptr, void *buf, size_t n);
void buffutils_file_seek(void *usrptr, size_t n, int whence);
void buffutils_file_close(void *usrptr);

#define file_writeback(file) {.usrptr = file, .write = buffutils_file_write, .close = buffutils_file_close}
#define file_readback(file) {.usrptr = file, .read = buffutils_file_read, .seek = buffutils_file_seek, .close = buffutils_file_close}

#endif