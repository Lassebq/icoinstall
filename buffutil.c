#include "buffutil.h"
#include "memfd.h"
#include <stdio.h>

void buffutils_memfd_write(void *usrptr, void *buf, size_t n) {
	memfile_write(buf, n, (MemFile *)usrptr);
}

int buffutils_memfd_read(void *usrptr, void *buf, size_t n) {
	return memfile_read(buf, n, (MemFile *)usrptr);
}

void buffutils_memfd_seek(void *usrptr, size_t n, int whence) {
	memfile_seek((MemFile *)usrptr, n, whence);
}

void buffutils_memfd_close(void *usrptr) {
}

void buffutils_file_write(void *usrptr, void *buf, size_t n) {
	fwrite(buf, n, 1, (FILE *)usrptr);
}

int buffutils_file_read(void *usrptr, void *buf, size_t n) {
	return fread(buf, n, 1, (FILE *)usrptr);
}

void buffutils_file_seek(void *usrptr, size_t n, int whence) {
	fseek((FILE *)usrptr, n, whence);
}

void buffutils_file_close(void *usrptr) {
	fclose((FILE *)usrptr);
}
