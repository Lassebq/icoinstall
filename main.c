#include "ico2png.h"
#include "xdgutil.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>
#include <unistd.h>
#include <pwd.h>
#include <errno.h>
#include <sys/stat.h>

void err(char *msg) {
	fprintf(stderr, "%s\n", msg);
}

FILE *fopen_mkdir(const char *path, const char *mode) {
	char *p = strdup(path);
	char *sep = strchr(p + 1, '/');
	while(sep != NULL) {
		*sep = '\0';
		if(mkdir(p, 0755) && errno != EEXIST) {
			fprintf(stderr, "error while trying to create %s\n", p);
		}
		*sep = '/';
		sep = strchr(sep + 1, '/');
	}
	free(p);
	return fopen(path, mode);
}

FILE *extract_filter(int index, int width, int height, int bitdepth,
					 void *filterdata) {
	if(bitdepth < 24) {
		return NULL;
	}
	char* name = (char *)filterdata;
	char iconpath[PATH_MAX];
	snprintf(iconpath, PATH_MAX,
			 "%s/icons/hicolor/%dx%d/apps/%s.png", XDG_DATA_HOME,
			 width, height, name);
	FILE *fd = fopen_mkdir(iconpath, "w");
	return fd;
}

int main(int argc, char **argv) {
	char *path, *name, *str;
	xdgutil_init();

	if(argc < 2) {
		printf("Usage: icoinstall [filename] [iconname]\n");
		printf("Extracts and installs specified windows icon to XDG_DATA_HOME/icons/hicolor\n");
		return EXIT_SUCCESS;
	}
	path = argv[1];
	if(access(path, R_OK) != 0) {
		printf("%s\n", path);
		err("File does not exist");
		return EXIT_FAILURE;
	}
	if(argc >= 3) {
		name = argv[2];
	} else {
		name = strdup(strrchr(path, '/') + 1);
		str = strrchr(name, '.');
		if(str) {
			// Remove .ico, or any extension
			*str = '\0';
		}
	}
	FILE *fd = fopen(path, "r");
	if(!fd) {
		err("An unexpected error occured");
		return EXIT_FAILURE;
	}
	ReadCallbacks cb2 = file_readback(fd);
	extract_icons(cb2, extract_filter, name);
	fclose(fd);
	return EXIT_SUCCESS;
}

