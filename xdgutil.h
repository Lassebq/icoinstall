#ifndef XDGUTIL_H
#define XDGUTIL_H

extern char* HOME;
extern char* XDG_DATA_HOME;
extern char* XDG_CONFIG_HOME;
extern char* XDG_CACHE_HOME;

void xdgutil_init();

#endif