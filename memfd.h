#ifndef MEMFD_H
#define MEMFD_H

#include <stdint.h>
#include <stdlib.h>

typedef struct {
	uint8_t *buff;
	size_t offset;
	size_t buffsz;
} MemFile;

#define new_memfd(size) {.buff = malloc(size), .offset = 0, .buffsz = size}
#define new_buff_memfd(buffer, size) {.buff = buffer, .offset = 0, .buffsz = size}

void memfile_write(void *buf, size_t n, MemFile *mf);
int memfile_read(void *buf, size_t n, MemFile *mf);
void memfile_seek(MemFile *mf, size_t n, int whence);

#endif