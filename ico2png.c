#include "ico2png.h"
#include "buffutil.h"
#include "config.h"
#include "memfd.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#if HAVE_PNG_H
#include <png.h>
#elif HAVE_LIBPNG_PNG_H
#include <libpng/png.h>
#elif HAVE_LIBPNG10_PNG_H
#include <libpng10/png.h>
#elif HAVE_LIBPNG12_PNG_H
#include <libpng12/png.h>
#endif

#define PNG_MAGIC 0x474e5089
#define ROW_BYTES(bits) ((((bits) + 31) >> 5) << 2)

static uint32_t simple_vec(uint8_t *data, uint32_t max_ofs, uint32_t ofs,
						   uint8_t size) {
	switch(size) {
		case 1:
			if((ofs / 8) >= max_ofs)
				return 0;
			return (data[ofs / 8] >> (7 - ofs % 8)) & 1;
		case 2:
			if((ofs / 4) >= max_ofs)
				return 0;
			return (data[ofs / 4] >> ((3 - ofs % 4) << 1)) & 3;
		case 4:
			if((ofs / 2) >= max_ofs)
				return 0;
			return (data[ofs / 2] >> ((1 - ofs % 2) << 2)) & 15;
		case 8:
			if(ofs >= max_ofs)
				return 0;
			return data[ofs];
		case 16:
			if((2 * ofs + 1) >= max_ofs)
				return 0;
			return data[2 * ofs] | data[2 * ofs + 1] << 8;
		case 24:
			if((3 * ofs + 2) >= max_ofs)
				return 0;
			return data[3 * ofs] | data[3 * ofs + 1] << 8 |
				   data[3 * ofs + 2] << 16;
		case 32:
			if((4 * ofs + 3) >= max_ofs)
				return 0;
			return data[4 * ofs] | data[4 * ofs + 1] << 8 |
				   data[4 * ofs + 2] << 16 | data[4 * ofs + 3] << 24;
	}

	return 0;
}

static void png_read_mem(png_structp png, png_bytep data, png_size_t size) {
	MemFile *io = (MemFile *)png_get_io_ptr(png);
	memfile_read(data, size, io);
}

static bool read_png_meta(uint8_t *image_data, uint32_t image_size,
						  int *bit_count, uint32_t *width, uint32_t *height) {
	png_structp png_ptr;
	png_infop info_ptr;
	png_byte ct;

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL,
									 NULL /*user_error_fn, user_warning_fn*/);
	if(png_ptr == NULL) {
		return false;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if(info_ptr == NULL) {
		return false;
	}

	if(setjmp(png_jmpbuf(png_ptr))) {
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return false;
	}

	MemFile png_in = new_buff_memfd(image_data, image_size);

	png_set_read_fn(png_ptr, &png_in, &png_read_mem);
	png_read_info(png_ptr, info_ptr);
	png_read_update_info(png_ptr, info_ptr);

	*width = png_get_image_width(png_ptr, info_ptr);
	*height = png_get_image_height(png_ptr, info_ptr);
	ct = png_get_color_type(png_ptr, info_ptr);
	if(ct & PNG_COLOR_MASK_PALETTE) {
		*bit_count = png_get_bit_depth(png_ptr, info_ptr);
	} else
		*bit_count = png_get_bit_depth(png_ptr, info_ptr) *
					 png_get_channels(png_ptr, info_ptr);

	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	return true;
}

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

int extract_icons(ReadCallbacks cb, ExtractFilter filter, void *filterdata) {
	int c, i;
	uint32_t palette_count = 0;
	uint32_t offset;
	uint32_t image_size, mask_size;
	uint8_t *image_data = NULL, *mask_data = NULL;
	int32_t width, height;
	IconDir *iconDir;
	IconDirEntry entry;
	uint8_t *data;
	BitmapInfoHeader *bitmap;
	FILE *out = NULL;
	RGBQuad *palette = NULL;

	png_structp png_ptr = NULL;
	png_infop info_ptr = NULL;
	png_byte *row = NULL;
	uint32_t d;

	iconDir = malloc(sizeof(IconDir));
	cb.read(cb.usrptr, iconDir, sizeof(IconDir));
	iconDir = realloc(iconDir, (sizeof(IconDir)) +
								   sizeof(IconDirEntry) * iconDir->count);

	cb.read(cb.usrptr, &iconDir->entries,
			iconDir->count * sizeof(IconDirEntry));
	for(i = 0; i < iconDir->count; i++) {
		entry = iconDir->entries[i];
		data = malloc(entry.bytesInRes);
		bitmap = (BitmapInfoHeader *)data;
		// Seek to the location in the file that has the image
		cb.seek(cb.usrptr, entry.imageOffset, SEEK_SET);
		cb.read(cb.usrptr, data, entry.bytesInRes);
		if(bitmap->size == PNG_MAGIC) {

			uint32_t unsigned_width, unsigned_height;
			int bit_count;

			if(!read_png_meta(data, entry.bytesInRes, &bit_count,
							  &unsigned_width, &unsigned_height))
				goto dib;

			out = filter(i, unsigned_width, unsigned_height, bit_count,
						 filterdata);
			if(out) {
				c++;
				fwrite(data, entry.bytesInRes, 1, out);
			}
			goto next;
		}
	dib:

		if(bitmap->size < sizeof(BitmapInfoHeader)) {
			fprintf(stderr, "Bitmap header is too small\n");
			goto next;
		}
		if(bitmap->x_pels_per_meter != 0) {
			fprintf(stderr, "x pixels per meter should be 0\n");
		}
		if(bitmap->y_pels_per_meter != 0) {
			fprintf(stderr, "y pixels per meter should be 0\n");
		}
		if(bitmap->compression != 0) {
			fprintf(stderr, "Compression is not supported\n");
			goto next;
		}
		if(bitmap->clr_important != 0) {
			fprintf(stderr, "clr_important should be 0\n");
		}
		if(bitmap->planes != 1) {
			fprintf(stderr, "planes should be 1\n");
		}
		offset = 0;
		if(bitmap->size != sizeof(BitmapInfoHeader)) {
			offset += bitmap->size - sizeof(BitmapInfoHeader);
		}
		offset += bitmap->size;
		palette_count = 0;
		if(bitmap->clr_used != 0 || bitmap->bit_count < 24) {
			palette_count =
				(bitmap->clr_used != 0 ? bitmap->clr_used
									   : (uint32_t)(1 << bitmap->bit_count));
			if(palette_count > 256) {
				fprintf(stderr, "palette too large");
				goto next;
			}
			palette = (RGBQuad *)(data + offset);
			offset += sizeof(RGBQuad) * palette_count;
		}
		if(abs(bitmap->width) > INT32_MAX / MAX(4, bitmap->bit_count)) {
			fprintf(stderr, "bitmap width too large");
			goto next;
		}

		width = bitmap->width;
		height = abs(bitmap->height) / 2;

		image_size = height * ROW_BYTES(width * bitmap->bit_count);
		mask_size = height * ROW_BYTES(width);

		image_data = (uint8_t *)(data + offset);
		offset += image_size;
		mask_data = (uint8_t *)(data + offset);
		offset += mask_size;

		if(entry.bytesInRes != bitmap->size + image_size + mask_size +
								   palette_count * sizeof(RGBQuad)) {
			fprintf(stderr,
					"incorrect total size of bitmap (%u specified; %ld real)",
					entry.bytesInRes,
					bitmap->size + image_size + mask_size +
						palette_count * sizeof(RGBQuad));
			goto next;
		}

		out = filter(i, width, height, entry.bitCount, filterdata);
		if(!out) {
			goto next;
		}
		png_ptr =
			png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if(!png_ptr) {
			goto next;
		}
		info_ptr = png_create_info_struct(png_ptr);
		if(!info_ptr) {
			goto next;
		}
		png_init_io(png_ptr, out);

		png_set_IHDR(png_ptr, info_ptr, width, height, 8,
					 PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
					 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
		png_write_info(png_ptr, info_ptr);

		row = malloc(width * 4);

		for(d = 0; d < (uint32_t)height; d++) {
			uint32_t x;
			uint32_t y = (bitmap->height < 0 ? d : height - d - 1);
			uint32_t imod = y * (image_size / height) * 8 / bitmap->bit_count;
			uint32_t mmod = y * (mask_size / height) * 8;

			for(x = 0; x < (uint32_t)width; x++) {
				uint32_t color = simple_vec(image_data, image_size, x + imod,
											bitmap->bit_count);

				if(bitmap->bit_count <= 16) {
					if(color >= palette_count) {
						fprintf(stderr, "color out of range in image data");
						goto next;
					}
					row[4 * x + 0] = palette[color].red;
					row[4 * x + 1] = palette[color].green;
					row[4 * x + 2] = palette[color].blue;
				} else {
					row[4 * x + 0] = (color >> 16) & 0xFF;
					row[4 * x + 1] = (color >> 8) & 0xFF;
					row[4 * x + 2] = (color >> 0) & 0xFF;
				}
				if(bitmap->bit_count == 32)
					row[4 * x + 3] = (color >> 24) & 0xFF;
				else
					row[4 * x + 3] =
						simple_vec(mask_data, mask_size, x + mmod, 1) ? 0
																	  : 0xFF;
			}

			png_write_row(png_ptr, row);
		}
		png_write_end(png_ptr, info_ptr);
		png_destroy_write_struct(&png_ptr, &info_ptr);
		c++;

	next:
		if(row) {
			free(row);
		}
		row = NULL;
		if(out) {
			fclose(out);
		}
		free(bitmap);
	}
	free(iconDir);
	return c;
}
