#include "memfd.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void memfile_write(void *buf, size_t n, MemFile *mf) {
	if(mf->offset + n >= mf->buffsz) {
		mf->buffsz *= 2;
		mf->buff = realloc(mf->buff, mf->buffsz);
	}
	memcpy(mf->buff + mf->offset, buf, n);
	mf->offset += n;
}

int memfile_read(void *buf, size_t n, MemFile *mf) {
	if(mf->offset + n >= mf->buffsz) {
		return 0;
	}
	memcpy(buf, mf->buff + mf->offset, n);
	mf->offset += n;
	return 1;
}

void memfile_seek(MemFile *memfd, size_t n, int whence) {
	switch (whence) {
		case SEEK_CUR:
		memfd->offset += n;
		return;
		case SEEK_SET:
		memfd->offset = n;
		return;
		case SEEK_END:
		fprintf(stderr, "memfd does not support SEEK_END\n");
		return;
	}
}
