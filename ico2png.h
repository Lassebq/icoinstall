#ifndef ICO2PNG_H
#define ICO2PNG_H
#include "buffutil.h"
#include <stdint.h>
#include <stdio.h>

typedef struct {
	uint8_t width;
	uint8_t height;
	uint8_t colorCount;
	uint8_t reserved;
	uint16_t planes;
	uint16_t bitCount;
	uint32_t bytesInRes;
	uint32_t imageOffset;
} IconDirEntry;

#pragma pack(push, 1)

typedef struct {
	uint16_t reserved;
	uint16_t type;
	uint16_t count;
	IconDirEntry entries[];
} IconDir;

#pragma pack(pop)

typedef struct {
	uint32_t size;
	int32_t width;
	int32_t height;
	uint16_t planes;
	uint16_t bit_count;
	uint32_t compression;
	uint32_t size_image;
	int32_t x_pels_per_meter;
	int32_t y_pels_per_meter;
	uint32_t clr_used;
	uint32_t clr_important;
} BitmapInfoHeader;

typedef struct {
	uint8_t blue;
	uint8_t green;
	uint8_t red;
	uint8_t reserved;
} RGBQuad;

typedef FILE *(ExtractFilter)(int index, int width, int height, int bitdepth, void *filterdata);

int extract_icons(ReadCallbacks cb, ExtractFilter filter, void *filterdata);
#endif