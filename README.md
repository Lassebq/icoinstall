# icoinstall
icoinstall is Linux a utility which extracts Windows ICO files and converts them to PNGs which are placed in `$XDG_DATA_HOME/icons/hicolor` to be used by application launchers.

This project depends on `libpng`